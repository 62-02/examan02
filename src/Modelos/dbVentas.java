/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
/**
 *
 * @author felip
 */
public class dbVentas extends dbManejador implements dbPersistencia{
    @Override
    public void insertar(Object objeto) throws Exception {
        Ventas user = new Ventas();
        user = (Ventas) objeto;
        String consulta = "insert into " + "ventas(codigo, tipo, precio, cantidad, total) values (?, ?, ?, ?, ?)";
        if(this.conectar()){
            try{
                System.err.println("Se conecto");
                this.sqlConsulta = conexion.prepareStatement(consulta);
                
                this.sqlConsulta.setString(1, user.getCodigo());
                this.sqlConsulta.setInt(2, user.getTipo());
                this.sqlConsulta.setDouble(3, user.getPrecio());
                this.sqlConsulta.setInt(4, user.getCantidad());
                this.sqlConsulta.setDouble(5, user.getTotal());
                this.sqlConsulta.executeUpdate();
                this.desconectar();
            }
            catch (SQLException e){
                System.err.println("Surgió un error al insertar" + e.getMessage());
            }
        }
        else{
            System.out.println("No fue posible conectarse ");
        }
    }

    @Override
    public void actualizar(Object objeto) throws Exception {
        Ventas user = new Ventas();
        user = (Ventas) objeto;
        String consulta = "update ventas set tipo = ?, precio = ?, cantidad = ?, total = ? where codigo = ?";
        if(this.conectar()){
            try{
                System.err.println("Se conecto");
                this.sqlConsulta = conexion.prepareStatement(consulta);


                this.sqlConsulta.setInt(1, user.getTipo());
                this.sqlConsulta.setDouble(2, user.getPrecio());
                this.sqlConsulta.setInt(3, user.getCantidad());
                this.sqlConsulta.setDouble(4, user.getTotal());
                this.sqlConsulta.setString(5, user.getCodigo());
                
                this.sqlConsulta.executeUpdate();
                this.desconectar();
            }
            catch (SQLException e){
                System.err.println("Surgió un error al actualizar" + e.getMessage());
            }
        }
        else{
            System.out.println("No fue posible conectarse ");
        }
    }

    @Override
    public boolean isExiste(String codigo) throws Exception {
        boolean confirm = false;
        if(this.conectar()){
            String consulta = "Select * from ventas where codigo = ?";
            this.sqlConsulta = conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, codigo);
            this.registros = this.sqlConsulta.executeQuery();
            if(this.registros.next()){
                confirm = true;
            }
        }
        this.desconectar();
        return confirm;
    }

    @Override
    public ArrayList listar() throws Exception {
        ArrayList<Ventas> lista = new ArrayList<Ventas>();
        Ventas user;
        if(this.conectar()){
            String consulta = "Select * from ventas order by codigo";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.registros = this.sqlConsulta.executeQuery();
            
            while(this.registros.next()){
                user = new Ventas();
                user.setCodigo(this.registros.getString("codigo"));
                user.setCantidad(this.registros.getInt("cantidad"));
                user.setTipo(this.registros.getInt("tipo"));
                user.setPrecio(this.registros.getDouble("precio"));
                user.setTotal(this.registros.getDouble("total"));

                lista.add(user);
            }
        }
        this.desconectar();
        return lista;
    }
    
    @Override
    public Object buscar(String codigo) throws Exception {
        Ventas user = new Ventas();
        if(conectar()){
            String consulta = "SELECT * from ventas WHERE codigo = ?";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, codigo);

            this.registros = this.sqlConsulta.executeQuery();
            if(this.registros.next()){
                user.setCodigo(this.registros.getString("codigo"));
                user.setPrecio(this.registros.getDouble("precio"));
                user.setTipo(this.registros.getInt("tipo"));
                user.setCantidad(this.registros.getInt("cantidad"));
                user.setTotal(this.registros.getDouble("total"));
            }
        }
        this.desconectar();
        return user;
    }
}
