/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;
    
/**
 *
 * @author felip
 */
public class Ventas {
    private int id;
    private String codigo;
    private double precio;
    private int cantidad;
    private int tipo;
    private double total;

    public Ventas(int id, String codigo, double precio, int cantidad, int tipo, double total) {
        this.id = id;
        this.codigo = codigo;
        this.precio = precio;
        this.cantidad = cantidad;
        this.tipo = tipo;
        this.total = total;
    }

    public Ventas() {
        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }
    
    public double CalcularTotal(){
        return this.precio * this.cantidad;
    }
}
