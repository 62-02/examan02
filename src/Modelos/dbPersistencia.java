/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;
import java.util.ArrayList;

/**
 *
 * @author felip
 */
public interface dbPersistencia {
    public void insertar(Object objeto) throws Exception;
    public void actualizar(Object objeto) throws Exception;
    
    public boolean isExiste(String codigo) throws Exception;
    public ArrayList listar() throws Exception;
    
    public Object buscar(String codigo) throws Exception;
}
