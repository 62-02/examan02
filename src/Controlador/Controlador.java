package Controlador;
import Vistas.dlgVentas;
import Vistas.dlgRegistros;
import Modelos.Ventas;
import Modelos.dbVentas;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
/**
 *
 * @author felip
 */
public class Controlador implements ActionListener{

    private dlgRegistros DR;
    private dlgVentas DV;
    private Ventas V;
    private dbVentas VD;
    private int bombRegular = 500;
    private int bombPremium = 500;
    public int terminado;
    private boolean option = false;
    private String antiguo;
    private String antiguo2;

    public Controlador(Ventas V, dbVentas VD, dlgRegistros DR, dlgVentas DV) {
        this.V = V;
        this.VD = VD;
        this.DV = DV;
        this.DR = DR;
        
        DV.btnVenta.addActionListener(this);
        DV.btnGuardar.addActionListener(this);
        DV.btnBuscar.addActionListener(this);
        DV.btnNuevo.addActionListener(this);
        DV.btnRegistros.addActionListener(this);
        DV.btnCerrar.addActionListener(this);
        DV.btnBombas.addActionListener(this);
        
        DR.btnMostrar.addActionListener(this);
        DR.btnRegresar.addActionListener(this);
    }

    private void iniciarVentas() {
        limpiar();
        DV.setTitle(":: VENTAS ::");
        DV.setSize(730,650);
        DV.setLocationRelativeTo(null);
        DV.setVisible(true);
    }
    
    private void iniciarRegistros() {
        DR.setTitle(":: REGISTROS ::");
        DR.setSize(730, 650);
        DR.setLocationRelativeTo(null);
        DR.setVisible(true);
    }
    
     private void cerrarVentas() {
        DV.setVisible(false);
    }
    
    private void cerrarRegistros() {
        DR.setVisible(false);
    }
    
    private void limpiar() {
        DV.txtCodigo.setText("");
        DV.txtCantidad.setText("");
        DV.cmbTipo.setSelectedIndex(0);
        
        DV.lblCantidad.setText("");
        DV.lblPrecio.setText("");
        DV.lblTotal.setText("");
        
        DV.cmbTipo.setEnabled(false);
        DV.txtCantidad.setEnabled(false);
        DV.txtCodigo.setEnabled(false);
        DV.cmbTipo.setEnabled(false);
        DV.btnVenta.setEnabled(false);
        DV.btnGuardar.setEnabled(false);
    }

    private void cargarDatos(){
        DefaultTableModel modelo = new DefaultTableModel();
        ArrayList<Ventas> lista = new ArrayList<>();
        try {
            lista = VD.listar();
        } catch (Exception ex) {
        }
        modelo.addColumn("Codigo");
        modelo.addColumn("Cantidad");
        modelo.addColumn("TipoGasolina");
        modelo.addColumn("Precio por LT");
        modelo.addColumn("Total");
        for (Ventas producto : lista) {
            modelo.addRow(new Object[]{producto.getCodigo(), producto.getCantidad(), producto.getTipo(), producto.getPrecio(), producto.getTotal()});
        }
        DR.tblVentas.setModel(modelo);
    }
    
   

    @Override
    public void actionPerformed(ActionEvent e) {
       
        if (e.getSource() == DV.btnGuardar) {
            /*try {
                Ventas ve = new Ventas();
                if("".equalsIgnoreCase(DV.txtCodigo.getText()))
                {
                    throw new IllegalArgumentException("Escribe un codigo");
                }
                
                if(Integer.parseInt(DV.txtCodigo.getText()) < 0)
                {
                    throw new IllegalArgumentException("Escribe un codigo");
                }
                
                if("".equalsIgnoreCase(DV.txtCantidad.getText()))
                {
                    throw new IllegalArgumentException("Escribe una cantidad");
                }
                
                if(Integer.parseInt(DV.txtCantidad.getText()) < 0)
                {
                    throw new IllegalArgumentException("Escribe un codigo");
                }
                
            }catch(IllegalArgumentException ex) {
                JOptionPane.showMessageDialog(DV, "SURGIO UN ERROR: " + ex.getMessage());
                return;
            }catch (Exception ex2) {
                JOptionPane.showMessageDialog(DV, "Error: " + ex2.getMessage());
                return;
            }
             try{
                V.setCodigo(DV.txtCodigo.getText());
                V.setTotal(V.CalcularTotal());
                V.setTipo(DV.cmbTipo.getSelectedIndex()+1);
                if(V.getTipo() == 1){
                    V.setPrecio(20.5);
                    
                }
                if(V.getTipo() == 2){
                    V.setPrecio(24.5);
                   
                }
                
                if((VD.isExiste(DV.txtCodigo.getText()))){
                A = true;
                if (A) {
                    VD.insertar(V);
                    JOptionPane.showMessageDialog(null, "Registro Guardado");
                    limpiar();
                }
                else {
                    VD.actualizar(V);
                    JOptionPane.showMessageDialog(null, "Registro Actulizado");
                    limpiar();
                    }
                }
            } catch (SQLException d) {
                d.printStackTrace();
            } catch (Exception ex1) {
                System.err.println("Surgió un error" + ex1);
            }*/
            
            try{
                if(option == true){
                   int choice = JOptionPane.showConfirmDialog(DV, "¿ACTUALIZAR?");
                   if(choice == 0){
                       V.setCodigo(DV.txtCodigo.getText());
                       V.setTipo(DV.cmbTipo.getSelectedIndex()+1);
                        if(V.getTipo() == 1){
                            V.setPrecio(20.5);
                            if(Integer.parseInt(DV.txtCantidad.getText()) <= this.bombRegular){
                                 V.setCantidad(Integer.parseInt(DV.txtCantidad.getText()));  
                                 if(Integer.parseInt(antiguo) < Integer.parseInt(DV.txtCantidad.getText())){
                                     this.bombRegular = this.bombRegular - (Integer.parseInt(DV.txtCantidad.getText()) - Integer.parseInt(antiguo));
                                 }
                                 JOptionPane.showMessageDialog(DV, "CANTIDAD EN RESERVA (R): " + this.bombRegular);
                            }
                            else{
                                JOptionPane.showMessageDialog(DV, "CANTIDAD NO DISPONIBLE");
                                return;
                            }
                        }
                        if(V.getTipo() == 2){
                            V.setPrecio(24.5);
                            if(Integer.parseInt(DV.txtCantidad.getText()) <= this.bombPremium){
                                 V.setCantidad(Integer.parseInt(DV.txtCantidad.getText()));
                                 if(Integer.parseInt(antiguo) < Integer.parseInt(DV.txtCantidad.getText())){
                                     this.bombPremium = this.bombPremium - (Integer.parseInt(DV.txtCantidad.getText()) - Integer.parseInt(antiguo));
                                 }
                                 JOptionPane.showMessageDialog(DV, "CANTIDAD EN RESERVA (P): " + this.bombPremium);
                            }
                            else{
                                JOptionPane.showMessageDialog(DV, "CANTIDAD NO DISPONIBLE");
                                return;
                            }

                        }
                       V.setTotal(V.CalcularTotal());
                       VD.actualizar(V);
                       limpiar();
                   }
                   option = false;
                   limpiar();
                   cargarDatos();
                   return;
                }
                if(!DV.txtCodigo.getText().equals("") && VD.isExiste(DV.txtCodigo.getText()) == false){
                    V.setCodigo(DV.txtCodigo.getText());
                    V.setTipo(DV.cmbTipo.getSelectedIndex()+1);
                        if(V.getTipo() == 1){
                            V.setPrecio(20.5);
                            if(Integer.parseInt(DV.txtCantidad.getText()) <= this.bombRegular){
                                 V.setCantidad(Integer.parseInt(DV.txtCantidad.getText()));  
                                 this.bombRegular = this.bombRegular - (Integer.parseInt(DV.txtCantidad.getText()));
                                 JOptionPane.showMessageDialog(DV, "CANTIDAD EN RESERVA (R): " + this.bombRegular);
                            }
                            else{
                                JOptionPane.showMessageDialog(DV, "CANTIDAD NO DISPONIBLE");
                                return;
                            }
                        }
                        if(V.getTipo() == 2){
                            V.setPrecio(24.5);
                            if(Integer.parseInt(DV.txtCantidad.getText()) <= this.bombPremium){
                                 V.setCantidad(Integer.parseInt(DV.txtCantidad.getText()));
                                 this.bombPremium = this.bombPremium - (Integer.parseInt(DV.txtCantidad.getText()));
                                 JOptionPane.showMessageDialog(DV, "CANTIDAD EN RESERVA (P): " + this.bombPremium);
                            }
                            else{
                                JOptionPane.showMessageDialog(DV, "CANTIDAD NO DISPONIBLE");
                                return;
                            }
                        }
                    JOptionPane.showMessageDialog(DV, "AGREGADO");
                    V.setTotal(V.CalcularTotal());
                    VD.insertar(V);
                    limpiar();
                    cargarDatos();       
                }
                else{
                    JOptionPane.showMessageDialog(DV, "VALOR INVÁLIDO");
                }
            }
            catch(Exception ex){
                System.out.println("Surgió un error" + ex.getMessage());
            }
        }
        
        if(e.getSource() == DV.btnBuscar){
           try {
                Ventas ve = new Ventas();
                if (VD.isExiste(DV.txtCodigo.getText())) {
                    ve = (Ventas) VD.buscar(DV.txtCodigo.getText());
                    DV.txtCodigo.setEnabled(false);
                    DV.txtCantidad.setText(Integer.toString(ve.getCantidad()));
                    DV.lblCantidad.setText(Integer.toString(ve.getCantidad()));
                    DV.lblPrecio.setText(Double.toString(ve.getPrecio()));
                    DV.lblTotal.setText(Double.toString(ve.getTotal()));
                    antiguo = DV.txtCantidad.getText();
                    if (ve.getTipo()== 1) {
                        DV.cmbTipo.setSelectedIndex(0);
                    }
                     if (ve.getTipo()== 2) {
                        DV.cmbTipo.setSelectedIndex(1);
                    }
                    JOptionPane.showMessageDialog(DV, "Se localizo");
                    option = true;
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(DV, "codigo no existe");
                System.out.println("Surgió un error" + ex.getMessage());
            }
        }
        
        if(e.getSource() == DV.btnVenta){
            DV.lblCantidad.setText(DV.txtCantidad.getText());
            V.setCantidad(Integer.parseInt(DV.txtCantidad.getText()));
            
            if(DV.cmbTipo.getSelectedIndex()+1 == 1){
                    V.setPrecio(20.5);
                }
                if(DV.cmbTipo.getSelectedIndex()+1 == 2){
                    V.setPrecio(24.5);
                }
            DV.lblPrecio.setText(Double.toString(V.getPrecio()));
            DV.lblTotal.setText(Double.toString(V.CalcularTotal()));
        }
        
        if(e.getSource() == DV.btnNuevo){
            limpiar();
            DV.cmbTipo.setEnabled(true);
            DV.txtCantidad.setEnabled(true);
            DV.txtCodigo.setEnabled(true);
            DV.cmbTipo.setEnabled(true);
            DV.btnVenta.setEnabled(true);
            DV.btnGuardar.setEnabled(true);
        }
        
        if(e.getSource() == DV.btnRegistros){
             cerrarVentas();
             iniciarRegistros();
        }
        
        if(e.getSource() == DV.btnCerrar){
            int out = JOptionPane.showConfirmDialog(DV, "¿Desea salir del programa?", "CERRAR PROGRAMA",JOptionPane.OK_CANCEL_OPTION);
            if(out == JOptionPane.OK_OPTION){
                System.exit(0); 
            }
        }
        
        if(e.getSource() == DV.btnBombas){
            if(this.bombPremium == 500 && this.bombRegular == 500){
                JOptionPane.showMessageDialog(DV, "BOMBAS LLENAS");
            }
            else{
                JOptionPane.showMessageDialog(DV, "SE RELLENARON LAS BOMBAS");
                this.bombPremium = 500;
                this.bombRegular = 500;
            }
        }
        
        if(e.getSource() == DR.btnRegresar){
             cerrarRegistros();
             iniciarVentas();
        }
        if(e.getSource() == DR.btnMostrar){
             cargarDatos();
        }
    }
        
    public static void main(String[] args) {
        Ventas V = new Ventas();
        dbVentas VD = new dbVentas();
        dlgRegistros DR = new dlgRegistros(new JFrame(), true);
        dlgVentas DV = new dlgVentas(new JFrame(), true);
        Controlador contra = new Controlador(V, VD, DR, DV);
        contra.iniciarVentas();
    }
}

